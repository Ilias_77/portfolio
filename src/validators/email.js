const regMail = /^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i;

export const email = ( message = "Неферный формат e-mail" ) => {
    return async (value) => (regMail.test(value) ? null : message);
}