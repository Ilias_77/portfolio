const regex = /^([a-zA-Zа-яА-Я]| )+$/i;

export const onlyLetters = ( message = "Допустимы только буквы" ) => {
    return async (value) => (regex.test(value) ? null : message);
}