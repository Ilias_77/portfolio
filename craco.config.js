const path = require('path');

module.exports = {
  webpack: {
    alias: {        
      '@': path.resolve(__dirname, 'src'),
      '@hooks': path.resolve(__dirname, './src/hooks'),
      '@store': path.resolve(__dirname, './src/store'),
      '@components': path.resolve(__dirname, './src/components'),
    },
  },
};