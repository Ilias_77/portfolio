const regPhone = /^\+?(\d{1,3})?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;
const regMail = /^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i;

export const phoneOrEmail = ( message = "Должен быть либо телефон, либо e-mail" ) => {
    return async (value) => (regPhone.test(value) || regMail.test(value) ? null : message);
}