import { addManyWorksAction } from "../store/workReducer"
const URL = "https://my-json-server.typicode.com/ZirIlias/jsonplaceholder/";
export const fetchWorks = () => {
    return (dispatch) => {
        fetch(URL+'posts')
            .then(response => response.json())
            .then(json => {
                dispatch(addManyWorksAction(json));
            })
    }
}