export const useAge = (dateString) => {
  const birthDate = new Date(dateString),
    today = new Date();

  let age = today.getFullYear() - birthDate.getFullYear(),
    m = today.getMonth() - birthDate.getMonth(),
    d = today.getDay() - birthDate.getDay();

  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  if (age === 0) {
    m = 12 + m;
    if (d < 0 || (d === 0 && today.getDate() < birthDate.getDate())) {
      m--;
    }
  }

  return age ? age : m;
};
