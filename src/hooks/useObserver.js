import { useEffect, useRef } from "react";

export const useObserver = (ref, callbackShow, callbackHide) => {
  const observer = useRef();
  useEffect(() => {
    if (observer.current) {
      observer.current.disconnect();
    }
    var cb = function (entries, observer) {
      if (entries[0].isIntersecting) {
        callbackShow();
      } else {
        callbackHide();
      }
    };
    observer.current = new IntersectionObserver(cb);
    observer.current.observe(ref.current);
    // eslint-disable-next-line
  }, []);
};
