const regPhone = /^\+?(\d{1,3})?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;

export const phone = ( message = "Неферный формат телефона" ) => {
    return async (value) => (regPhone.test(value) ? null : message);
}